class Gallery {
  constructor(gallery) {
    this.photos = [];
    this.pagesAmount = 0;
    this.currentPage = 1;
    this.photosPerPage = 15;

    this.galleryEl = gallery.querySelector('#gallery')
    this.fullSizeBoxEl = gallery.querySelector('#full-size-box')
    this.fullSizeBgEl = gallery.querySelector('#full-size-bg');
    this.pagesEl = gallery.querySelector('#pages');

    this.galleryEl.addEventListener('click', this.onGalleryClicked.bind(this));
    this.fullSizeBgEl.addEventListener('click', this.onFullSizeBgClicked.bind(this));
    this.pagesEl.addEventListener('click', this.onPagesClicked.bind(this));
  }

  onGalleryClicked(e) {
    if(e.target.classList.contains('photo')) {
      this.showFullSize(e.target);
    }
  }
  
  onFullSizeBgClicked() {
    this.switchFullSize();
  }
  
  onPagesClicked(e) {
    if(e.target.classList.contains('page')) {
      this.currentPage = e.target.innerText;
      this.renderPages();
      this.setActivePage();
      this.renderPhotos();
    }
  }

  set current(page) {
    this.currentPage = page;
  }

  init() {
    this.getPhotos().then(() => {
      this.getPagesAmount();
      this.restorePageFromStorage();
      this.renderPages();
      this.setActivePage();
      this.renderPhotos();
    })
  }

  getPhotos() {
    return fetch(Gallery.PHOTOS_URL)
      .then(response => response.json())
      .then(receivedPhotos => {this.photos = receivedPhotos}) 
  }

  getPagesAmount() {
    this.pagesAmount = Math.ceil(this.photos.length / this.photosPerPage);
  }

  renderPhotos() {
    const pagePhotos = this.getPagePhotos(this.currentPage);
    let photosHtml = '';
    
    this.savePageToStorage(this.currentPage);

    pagePhotos.forEach(photo => photosHtml += this.getPhotoHtml(photo));
  
    this.galleryEl.innerHTML = photosHtml;
  }

  getPagePhotos() {
    return this.photos.filter(photo => photo.id > (this.currentPage - 1) * this.photosPerPage 
                                       && photo.id <= this.currentPage * this.photosPerPage);
  }

  getPhotoHtml(photo) {
    return Gallery.photoTemplate.replace(Gallery.PHOTO_URL_PLACEHOLDER, photo.thumbnailUrl)
  }

  renderPages() {
    let pagesHtml = '';
  
    for(let i = 0; i < this.pagesAmount; i++) {

      if(i > 0 && i < +this.currentPage - 4){
        if(i === 1)
          pagesHtml += Gallery.skippedPagesTemplate;
        continue;
      }
      
      if(i < this.pagesAmount - 1 && i > +this.currentPage + 2){
        if(i === this.pagesAmount - 2)
          pagesHtml += Gallery.skippedPagesTemplate;
        continue;
      }

      pagesHtml += this.getPageHtml(i+1);
    }
  
    this.pagesEl.innerHTML = pagesHtml;
  }
  
  getPageHtml(page) {
    return Gallery.pageTemplate.replace(Gallery.PAGE_PLACEHOLDER, page)
  }
  
  setActivePage() {
    const pages = Array.from(this.pagesEl.children);
  
    this.unsetActivePage(pages);
  
    pages.find(el => el.innerText == this.currentPage)
         .classList.toggle('active');
  }
  
  unsetActivePage(pages){
    const activePage = pages.find(el => el.classList.contains('active'));
  
    if(activePage)
      activePage.classList.toggle('active');
  }

  showFullSize(photoEl) {
    const photo = this.photos.find(el => el.thumbnailUrl === photoEl.currentSrc);
    
    this.fullSizeBoxEl.innerHTML = this.getFullSizeHtml(photo);
    this.switchFullSize();
  }

  getFullSizeHtml(photo) {
    return Gallery.fullSizeTemplate.replace(Gallery.PHOTO_URL_PLACEHOLDER, photo.url);
  }

  switchFullSize() {
    this.fullSizeBoxEl.classList.toggle('full-size-disable');
    this.fullSizeBgEl.classList.toggle('full-size-disable');
  }

  savePageToStorage(page) {
    localStorage.setItem('galleryPage', `${page}`);
  }
  
  restorePageFromStorage(){
    this.currentPage = localStorage.getItem('galleryPage') || 1;
  }

  static PHOTOS_URL = 'https://jsonplaceholder.typicode.com/photos';
  static PHOTO_URL_PLACEHOLDER = '{{photoUrl}}';
  static PAGE_PLACEHOLDER = '{{page}}';
  static photoTemplate = document.getElementById('photoTemplate').innerHTML;
  static fullSizeTemplate = document.getElementById('fullSizeTemplate').innerHTML;
  static pageTemplate = document.getElementById('pageTemplate').innerHTML;
  static skippedPagesTemplate = document.getElementById('skippedPagesTemplate').innerHTML;
}
const galleryEl = document.getElementById('gallery-container');

const gallery = new Gallery(galleryEl);

gallery.init();