class Controller {
  constructor($root) {
    this.initList();
    this.initView($root);
  }

  initList() {
    this.collection = new Collection(TODOS_URL);
    this.collection.getList().then(() => this.renderList());
  }

  initView($root) {
    this.view = new View($root, {
      onDelete: this.deleteItem.bind(this),
      onAdd: this.addItem.bind(this),
      onToggle: this.toggleItem.bind(this),
    });
  }

  renderList() {
    this.view.renderList(this.collection.list);
  }

  deleteItem(id) {
    this.collection
      .deleteItem(id)
      .then(() => this.view.deleteItemEl(id));
  }

  addItem(newItem) {
    this.collection
      .addItem(newItem)
      .then((newItem) => this.view.renderNewItem(newItem));
  }

  toggleItem(id) {
    this.collection
      .toggleItem(id)
      .then(() => this.view.toggleItem(id));
  }
}
