const TODO_TEMPLATE = $('#taskTemplate').html();
const FORM_TEMPLATE = $('#formTemplate').html();
const ID_PLACEHOLDER = '{{id}}';
const TITLE_PLACEHOLDER = '{{title}}';
const ISDONE_PLACEHOLDER = '{{isDone}}';
const IS_DONE_TRUE = 'done';
const DELETE_BTN_SELECTOR = '.task-remove-button';
const ADD_BTN_SELECTOR = '#add-task-button';
const TASK_INPUT_SELECTOR = '#task-input';
const ITEM_SELECTOR = '.list-item';

class View {
  constructor($root, actions = {}) {
    this._$root = $root;
    this._actions = actions;
    this._$list = null;
    this._$addForm = null;

    this.initView();
  }

  initView() {
    this._$list = $('<div></div>').addClass('list-container');
    this._$list.on('click', DELETE_BTN_SELECTOR, this.onDeleteBtnClick.bind(this));
    this._$list.on('click', ITEM_SELECTOR, this.onItemClick.bind(this));
    this._$addForm = $(FORM_TEMPLATE).on(
      'click',
      ADD_BTN_SELECTOR,
      this.onFormClick.bind(this)
    );
    this._$root.append(this._$list);
    this._$root.append(this._$addForm);
  }

  onDeleteBtnClick(e) {
    const id = this.getElementId($(e.target));
    this._actions.onDelete(id);
  }

  onItemClick(e) {
    const id = this.getElementId($(e.target));
    this._actions.onToggle(id);
  }

  onFormClick() {
    const newItem = this.getNewItem();
    this.resetForm();
    this._actions.onAdd(newItem);
  }

  renderList(list) {
    this._$list.html(list.map(this.getItemHtml).join(''));
  }

  getItemHtml({ id, title, isDone }) {
    return TODO_TEMPLATE.replace(ID_PLACEHOLDER, id)
      .replace(TITLE_PLACEHOLDER, title)
      .replace(ISDONE_PLACEHOLDER, isDone ? IS_DONE_TRUE : '');
  }

  getElementId($el) {
    return $el.closest(ITEM_SELECTOR).data('taskId');
  }

  deleteItemEl(id) {
    this.getItemById(id).remove();
  }

  getItemById(id) {
    return $(`[data-task-id="${id}"]`);
  }

  getNewItem() {
    return {
      title: $(TASK_INPUT_SELECTOR).val(),
      isDone: false,
    };
  }

  resetForm() {
    $(TASK_INPUT_SELECTOR).val('');
  }

  renderNewItem(newItem) {
    this._$list.append($(this.getItemHtml(newItem)));
  }

  toggleItem(id) {
    this.getItemById(id).toggleClass('done');
  }
}
