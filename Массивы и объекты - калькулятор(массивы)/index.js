function getOperator() {
	let operator;

	do {
		operator = prompt('Enter operator: ');
	} while (operatorValidation(operator));

	return operator;
}

function operatorValidation(operator) {
	return !(operator === '+' || operator === '-' || operator === '*' || operator === '/');
}

function enterOperands() {
	let str = '';
	let operandsArray = [];

	do {
		str = prompt('Enter operands separated by commas:');
	} while (isAnswerEmpty(str))

	operandsArray = str.split(',');
	deleteInvalidOperands(operandsArray);

	return operandsArray.map(Number);
}

function isAnswerEmpty(str) {
	return str === null || str === '';
}

function deleteInvalidOperands(operandsArray) {
	do {
		operandsArray.forEach((element, index) => {
			if (isNaN(element)) {
				operandsArray.splice(index, 1);
			}
		});
	} while (arrayValidation(operandsArray))
}

function arrayValidation(operandsArray) {
	return operandsArray.some((element) => {
		return isNaN(element);
	});
}

function addition(arr) {
	return arr.reduce((result, current) => {
		return result + current;
	});
}

function subtraction(arr) {
	return arr.reduce((result, current) => {
		return result - current;
	})
}

function multiplication(arr) {
	return arr.reduce((result, current) => {
		return result * current;
	})
}

function division(arr) {
	return arr.reduce((result, current) => {
		return result / current;
	})
}

function calculate(operandsArray, operator) {
	switch(operator) {
		case '+':
			return addition(operandsArray);
		case '-':
			return subtraction(operandsArray);
		case '*':
			return multiplication(operandsArray);
		case '/':
			return division(operandsArray);
	}
}

function showResult(operandsArray, operator, result) {
	alert(`${operandsArray.join(operator)}=${result}`);
}

const operator = getOperator();
const operands = enterOperands();
const result = calculate(operands, operator);

showResult(operands, operator, result);
