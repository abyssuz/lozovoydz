function Calculator(initialValue) {
    this.value = initialValue;
    this.add = (a) => this.value += a;
    this.sub = (a) => this.value -= a;
    this.mult = (a) => this.value *= a;
    this.div = (a) => this.value /= a;
}

const resultOutput = document.getElementById('result');
const operandInput = document.getElementById('operand-input');

const initValue = getInitialValue();
renderOutput(initValue);
const calc = new Calculator(initValue);

document.getElementById('actions').addEventListener('click', onActionsClick);

function onActionsClick(event) {
    if(isInputValid()) {
        const actionResult = doAction(event.target);
        renderOutput(actionResult);
    } else {
        alert('Input invalid!')
    }
    resetInput();
}

function isInputValid() {
    return operandInput.value !== '' && !isNaN(operandInput.value);
}

function doAction(target) {
    const operand = +operandInput.value;

    switch(true) {
        case target.classList.contains('add'):
            return calc.add(operand);
        case target.classList.contains('sub'):
            return calc.sub(operand);
        case target.classList.contains('mult'):
            return calc.mult(operand);
        case target.classList.contains('div'):
            return calc.div(operand);
    }
}

function renderOutput(result) {
    resultOutput.textContent = result;
}

function resetInput(){
    operandInput.value = '';
}

function getInitialValue(){
    let initialValue = '';

    do {
        initialValue = prompt('Set initial value:');
    } while(isInitialValueValid(initialValue))

    return +initialValue;
}

function isInitialValueValid(str) {
    return str === '' || isNaN(str) || str === null;
}