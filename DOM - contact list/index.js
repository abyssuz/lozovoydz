const contactTable = document.getElementById('contact-table')
const nameInputEl = document.getElementById('name-input');
const lastNameInputEl = document.getElementById('lastname-input');
const phoneInputEl = document.getElementById('phone-input');
const contactTemplate = document.getElementById('contact-template').innerHTML;

contactTable.addEventListener('click', onContactTableClick);

document.getElementById('add-contact-button')
        .addEventListener('click', onAddContactBtnClick);


function onContactTableClick(e) {
	console.log(e);
    if(e.target.classList.contains('delete-button'))
        removeContact(e.target);
}

function onAddContactBtnClick() {
    if(isInputsValid()){
        addNewContact();
        resetInputs();
    } else {
        alert('Inputs are empty');
    }
}

function removeContact(element) {
    element.closest('.contact').remove();
}

function isInputsValid() {
    return nameInputEl.value !== '' && lastNameInputEl.value !== '' && phoneInputEl.value !== '';
}

function addNewContact(){
    const newContactHtml = getNewContactHtml();
    contactTable.insertAdjacentHTML('beforeend', newContactHtml);
}

function getNewContactHtml() {
    const newContactHtml = contactTemplate.replace('{{name}}', nameInputEl.value)
                                            .replace('{{lastname}}', lastNameInputEl.value)
                                            .replace('{{phone}}', phoneInputEl.value);
                                            
    return newContactHtml;
}

function resetInputs() {
    nameInputEl.value = '';
    lastNameInputEl.value = '';
    phoneInputEl.value = '';
}