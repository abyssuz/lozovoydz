function getAverageStudentMark(student) {
    return calculateAverageMark(student.marks);
}

function getAverageGroupMark(group) {
    let studentsMarks = [];

    group.forEach(element => studentsMarks = studentsMarks.concat(element.marks));

    return calculateAverageMark(studentsMarks);
}

function calculateAverageMark(marks) {
    return getMarksSum(marks) / marks.length;
}

function getMarksSum(marks) {
    return marks.reduce((result, current) => result + current);
}

const students = [
    {    
        name: 'John Smith',
        marks: [10, 8, 6, 9, 8, 7],
    },
    {
        name: 'John Doe',
        marks: [9, 8, 7, 6, 7],
    },
    {
        name: 'Thomas Anderson',
        marks: [6, 7, 10, 8],
    },
    {
        name: 'Jean-Baptiste Emanuel Zorg',
        marks: [10, 9, 8, 9],
    }
];

for (let i = 0; i < students.length; i++) {
    console.log(`${students[i].name}: ${getAverageStudentMark(students[i])}`);
}


console.log(`Group: ${getAverageGroupMark(students)}`);