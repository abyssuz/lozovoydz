function getAverageStudentMark(id) {
    const student = students.find(element => element.id === id)

    return calculateAverageMark(student.marks);
}

function getAverageGroupMark(group) {
    let studentsMarks = [];

    group.forEach(element => studentsMarks = studentsMarks.concat(element.marks));

    return calculateAverageMark(studentsMarks);
}

function calculateAverageMark(marks) {
    return getMarksSum(marks) / marks.length;
}

function getMarksSum(marks) {
    return marks.reduce((result, current) => result + current);
}

const students = [
    {
        id: 10,     
        name: 'John Smith',
        marks: [10, 8, 6, 9, 8, 7],
    },
    {
        id: 11,
        name: 'John Doe',
        marks: [9, 8, 7, 6, 7],
    },
    {
        id: 12,
        name: 'Thomas Anderson',
        marks: [6, 7, 10, 8],
    },
    {
        id: 13,
        name: 'Jean-Baptiste Emanuel Zorg',
        marks: [10, 9, 8, 9],
    }
];

console.log(getAverageStudentMark(12));
console.log(`Group: ${getAverageGroupMark(students)}`);