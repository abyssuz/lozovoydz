function getUserName(){
  let name = '';

  do {
    name = prompt('Enter your name:');
  } while(isNameEmpty(name))

  return name;
}

function isNameEmpty(str){
  return str === null || str === '';
}

const userName = getUserName();
document.body.innerHTML = `<h1>Hello, ${userName}!</h1>`;


