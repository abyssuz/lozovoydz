const STICKER_TEMPLATE = $('#sticker-template').html();
const DESC_PLACEHOLDER = '{{description}}';
const STICKERS_URL = 'https://5dd3d5ba8b5e080014dc4bfa.mockapi.io/stickers/';

const addBtn = $('#add-sticker');
const board = $('#sticker-container');

let stickerList = [];

addBtn.on('click', onAddBtnClicked);
board.on('click', '.sticker', onBoardClicked);
board.on('focusout', '.sticker-input', onBoardFocusout);

function onAddBtnClicked() {
  addNewSticker();
}

function onBoardClicked(e) {
  switch (true) {
    case $(e.target).hasClass('delete-btn'): {
      deleteSticker(e.target);
      break;
    }
    case $(e.target).hasClass('sticker') ||
      $(e.target).hasClass('sticker-desc'): {
      editSticker(e.target);
      break;
    }
  }
}

function onBoardFocusout(e) {
  saveEdit(e.target);
}

function init() {
  getStickers().then(renderList);
}

function getStickers() {
  return fetch(STICKERS_URL)
    .then((resp) => resp.json())
    .then((data) => (stickerList = data));
}

function renderList() {
  stickerList.forEach((sticker) => renderSticker(sticker));
}

function renderSticker(sticker) {
  const stickerHtml = getStickerHtml(sticker.description);

  const newSticker = $('<div></div>')
    .addClass('sticker')
    .attr('data-sticker-id', sticker.id)
    .html(stickerHtml);

  board.append(newSticker);
}

function getStickerHtml(description) {
  return STICKER_TEMPLATE.replace(DESC_PLACEHOLDER, description);
}

function addNewSticker() {
  const newSticker = { description: '' };

  fetch(STICKERS_URL, {
    method: 'POST',
    body: JSON.stringify(newSticker),
    headers: { 'Content-Type': 'application/json' },
  })
    .then((resp) => resp.json())
    .then((data) => {
      renderSticker(data);
      stickerList.push(data);
    });
}

function deleteSticker(stickerEl) {
  const stickerId = getStickerId(stickerEl);

  fetch(STICKERS_URL + stickerId, {
    method: 'DELETE',
  })
    .then(() => {
      deleteStickerEl(stickerId);
    })
    .then(() => {
      stickerList = stickerList.filter((sticker) => sticker.id !== stickerId);
    });
}

function getStickerId(element) {
  return $(element).closest('.sticker').attr('data-sticker-id');
}

function deleteStickerEl(id) {
  $(`[data-sticker-id="${id}"]`).remove();
}

function editSticker(stickerEl) {
  const stickerId = getStickerId(stickerEl);
  const stickerInput = $(`[data-sticker-id="${stickerId}"] :input`);
  const stickerDesc = getStickerDescEl(stickerId);

  stickerInput.toggleClass('active').val(stickerDesc.text()).focus();
  stickerDesc.text('');
}

function getStickerDescEl(id) {
  return $(`[data-sticker-id="${id}"] .sticker-desc`);
}

function saveEdit(inputEl) {
  const input = $(inputEl);
  const stickerId = getStickerId(input);
  const stickerDesc = getStickerDescEl(stickerId);
  const editedSticker = { id: stickerId, description: input.val() };

  fetch(STICKERS_URL + stickerId, {
    method: 'PUT',
    body: JSON.stringify(editedSticker),
    headers: { 'Content-Type': 'application/json' },
  })
    .then((resp) => resp.json())
    .then((data) => {
      stickerDesc.text(data.description);
      input.toggleClass('active').val('');
      updateList(data);
    });
}

function updateList(editedSticker) {
  const oldStickerIndex = stickerList.findIndex(
    (sticker) => editedSticker.id === sticker.id
  );

  stickerList.splice(oldStickerIndex, 1, editedSticker);
}

init();
