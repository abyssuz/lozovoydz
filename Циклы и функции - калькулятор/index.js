function getOperator() {
	let operator;
	do {
		operator = prompt('Enter operator: ');
	} while(!(operator === '+' || operator === '-' || operator === '*' || operator === '/'))
	return operator;
}

function numberValidation (number) {
		return isNaN(number) || (number === 0);
}

function enterOperand (operandIndex) {
	let operand;
	do {
		operand = +prompt(`Enter operand ${operandIndex}: `);
	} while(numberValidation(operand))
	return operand;
}

function calculate (a, b, operator) {
	switch(operator) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		case '/':
			return a / b;
	}
}

function showResult(a, b, operator) {
	alert(`${a} ${operator} ${b} = ${calculate(a, b, operator)}`);
}

const operator = getOperator();
const firstOperand = enterOperand(1);
const secondOperand = enterOperand(2);
showResult(firstOperand, secondOperand, operator);
