function getOperator() {
	let operator;
	do {
		operator = prompt('Enter operator: ');
	} while(operatorValidation(operator));
	return operator;
}

function operatorValidation(operator) {
	return !(operator === '+' || operator === '-' || operator === '*' || operator === '/');
}

function getOperandsAmount () {
	let amount;
	do {
		amount = +prompt('Enter amount of operands: ');
	} while(amountOperandsValidation(amount))
	return amount;
}

function amountOperandsValidation(amount) {
	return isNaN(amount) || amount === null || amount < 1 || amount > 6;
}

function enterOperand (operandIndex) {
	let operand;
	do {
		operand = prompt(`Enter operand ${operandIndex}: `);
	} while(operandValidation(operand))
	return +operand;
}

function operandValidation (number) {
	return isNaN(number) || (number === '') || (number === null);
}

function addition(operands, amount) {
	let result = operands[0];
	
	for(let i = 1; i < amount; i++) {
		result += operands[i];
	}
	return result;
}

function subtraction(operands, amount) {
	let result = operands[0];
	
	for(let i = 1; i < amount; i++) {
		result -= operands[i];
	}
	return result;
}

function multiplication(operands, amount) {
	let result = operands[0];
	
	for(let i = 1; i < amount; i++) {
		result *= operands[i];
	}
	return result;
}

function division(operands, amount) {
	let result = operands[0];
	
	for(let i = 1; i < amount; i++) {
		result /= operands[i];
	}
	return result;
}

function calculate (operands, operator, amount) {
	switch(operator) {
		case '+':
			return addition(operands, amount);
		case '-':
			return subtraction(operands, amount);
		case '*':
			return multiplication(operands, amount);
		case '/':
			return division(operands, amount);
	}
}

function showResult(operands, operator, operationResult, amount) {
	let resultExpression = '';
	
	for(let i = 0; i < amount; i++) {
		resultExpression += operands[i];
		if(i + 1 !== amount)
			resultExpression += operator;
	}
	alert(resultExpression + `=${operationResult}`);
}

const operator = getOperator();
const amountOperands = getOperandsAmount();
let operands = {};

for (let i = 0; i < amountOperands; i++) {
	operands[i] = enterOperand(i+1);
}

const result = calculate(operands, operator, amountOperands);
showResult(operands, operator, result, amountOperands);