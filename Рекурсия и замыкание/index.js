function createCalculator(a) {
  return calc = {
    add: (b) => a += b,
    sub: (b) => a -= b,
    mult: (b) => a *= b,
    div: (b) => a /= b,
    set: (b) => a = b,
  };
}
/*
	Условие такое что пользователь вводит 
	начальное значение, а после всегда вводит
	оператор и число без пробелов - "+2", "*5", 
	"=200" и результат выводится в консоль.
	Отмена или пустая строка = выход.
*/
const actions = {
  "+": (operand) => console.log(calculator.add(operand)),
  "-": (operand) => console.log(calculator.sub(operand)),
  "*": (operand) => console.log(calculator.mult(operand)),
  "/": (operand) => console.log(calculator.div(operand)),
  "=": (operand) => console.log(calculator.set(operand)),
  default: () => alert("Invalid operator!"),
};

function getAction() {
  let answer = prompt("What to do ?");

  if (answer !== null && answer !== "") {
    doAction(answer);
    getAction();
  } else 
    console.log("exit");
}

function doAction(answer) {
  const operator = answer[0];
  const operand = +answer.substring(1);
  const action = spotAction(operator);
  action(operand);
}

function spotAction(operator) {
  if (actions[operator]) 
    return actions[operator];
  else 
    return actions.default;
}

const initialValue = +prompt("Enter initial value: ");
console.log(initialValue);
const calculator = createCalculator(initialValue);
getAction();
