const photoTemplate = document.getElementById('photoTemplate').innerHTML;
const galleryElement = document.getElementById('gallery');
const PHOTO_URL_PLACEHOLDER = '{{photoUrl}}';

let photos = [];

function getPhotos() {
  fetch('https://jsonplaceholder.typicode.com/photos?albumId=1')
  .then((response) =>{
    response.json()
      .then((receivedPhotos) => {photos = receivedPhotos})
      .then(renderPhotos)
  })
}

function renderPhotos() {
  let photosHtml = '';

  photos.forEach(photo => {
    photosHtml += getPhotoHtml(photo);
  })

  galleryElement.insertAdjacentHTML('beforeend', photosHtml);
}

function getPhotoHtml(photo) {
  return photoTemplate.replace(PHOTO_URL_PLACEHOLDER, photo.thumbnailUrl)
}

getPhotos();
