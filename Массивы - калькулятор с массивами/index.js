function getOperator() {
	let operator;

	do {
		operator = prompt('Enter operator: ');
	} while (operatorValidation(operator));

	return operator;
}

function operatorValidation(operator) {
	return !(operator === '+' || operator === '-' || operator === '*' || operator === '/');
}

function getOperands() {
	let str = '';
	let operandsArray = [];

	do {
		str = prompt('Enter operands separated by commas:');
	} while (isAnswerEmpty(str))

	operandsArray = str.split(',');
	operandsArray = operandsArray.filter(element => element % 2 > 0 && !isNaN(element));
	
	return operandsArray.map(Number);
}

function isAnswerEmpty(str) {
	return str === null || str === '';
}

function calculate(array, operator) {
	switch(operator) {
		case '+':
			return addition(array);
		case '-':
			return subtraction(array);
		case '*':
			return multiplication(array);
		case '/':
			return division(array);
	}
}

function addition(arr) {
	return arr.reduce((result, current) => result + current);
}

function subtraction(arr) {
	return arr.reduce((result, current) => result - current);
}

function multiplication(arr) {
	return arr.reduce((result, current) => result * current);
}

function division(arr) {
	return arr.reduce((result, current) => result / current);
}

function showResult(operandsArray, operator) {
	alert(`${operandsArray.join(operator)}=${calculate(operands, operator)}`);
}

const operator = getOperator();
const operands = getOperands();

showResult(operands, operator);
