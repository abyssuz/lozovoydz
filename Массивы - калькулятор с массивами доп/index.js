function getExpression() {
	let str = '';

	do {
		str = prompt('Enter expression: ');
	} while (isAnswerEmpty(str))
	
	return str;
}

function isAnswerEmpty(str) {
	return str === null || str === '';
}

function getOperands(str) {
	let operandsArray = str.split(spotOperator(str));

	operandsArray = operandsArray.filter(element => element % 2 > 0 && !isNaN(element));

	return operandsArray.map(Number);
}

function spotOperator(exp) {
	if (exp.includes('+'))
		return '+';
	else if (exp.includes('-'))
		return '-';
	else if (exp.includes('*'))
		return '*';
	else if (exp.includes('/'))
		return '/';
}

function calculate(array, operator) {
	switch(operator) {
		case '+':
			return addition(array);
		case '-':
			return subtraction(array);
		case '*':
			return multiplication(array);
		case '/':
			return division(array);
	}
}

function addition(arr) {
	return arr.reduce((result, current) => result + current);
}

function subtraction(arr) {
	return arr.reduce((result, current) => result - current);
}

function multiplication(arr) {
	return arr.reduce((result, current) => result * current);
}

function division(arr) {
	return arr.reduce((result, current) => result / current);
}

function showResult(operands, operator) {
	alert(`${operands.join(operator)}=${calculate(operands, operator)}`);
}

const expression = getExpression();
const operands = getOperands(expression);
const operator = spotOperator(expression);

showResult(operands, operator);
