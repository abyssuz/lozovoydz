export default class Collection {
  constructor(albumsUrl, albumPhotosUrl) {
    this._albumsUrl = albumsUrl;
    this._albumPhotosUrl = albumPhotosUrl;
    this.albumsList = [];
    this.albumPhotos = [];
  }

  getAlbumsList() {
    return fetch(this._albumsUrl)
      .then((resp) => resp.json())
      .then((data) => (this.albumsList = data));
  }

  getAlbumPhotos(id = 1) {
    return fetch(this._albumPhotosUrl + id)
      .then((resp) => resp.json())
      .then((data) => (this.albumPhotos = data));
  }
}
