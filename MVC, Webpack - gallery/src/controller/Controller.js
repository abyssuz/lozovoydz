import { ALBUMS_URL, ALBUM_PHOTO_URL } from '../utility/constants';
import View from '../view/View';
import Collection from '../model/Collection';

export default class Controller {
  constructor(root) {
    this.initList();
    this.initView(root);
  }

  initList() {
    this.collection = new Collection(ALBUMS_URL, ALBUM_PHOTO_URL);
    this.collection.getAlbumsList().then(() => {
      this.collection.getAlbumPhotos().then(() => {
        this.renderGallery();
      });
    });
  }

  initView(root) {
    this.view = new View(root, {
      onAlbumChange: this.onAlbumChange.bind(this),
    });
  }

  renderGallery() {
    this.view.renderAlbums(this.collection.albumsList);
    this.renderAlbumPhotos();
  }

  renderAlbumPhotos() {
    this.view.renderAlbumPhotos(this.collection.albumPhotos);
  }

  onAlbumChange(id) {
    this.collection.getAlbumPhotos(id).then(() => {
      this.renderAlbumPhotos();
    });
  }
}