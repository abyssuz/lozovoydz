import $ from 'jquery';

const ALBUM_TEMPLATE = $('#albumTemplate').html();
const PHOTO_TEMPLATE = $('#photoTemplate').html();
const DIV_TEMPLATE = '<div></div>';
const ALBUM_TITLE_PLACEHOLDER = '{{albumTitle}}';
const ALBUM_ID_PLACEHOLDER = '{{id}}';
const PHOTO_URL_PLACEHOLDER = '{{url}}';
const GALLERY_CLASS = 'gallery';
const ALBUMS_BOX_CLASS = 'albums-box';
const PHOTOS_BOX_CLASS = 'photos-box';
const ACTIVE_CLASS = 'active';
const ALBUM_TITLE_SELECTOR = '.album-title';
const ACTIVE_ALBUM_SELECTOR = '.album-title.active';
const DATA_SELECTOR = '[data-album-id={{id}}]';

export default class View {
  constructor(root, actions = {}) {
    this._$root = $(root);
    this._actions = actions;
    this._$gallery = null;
    this._$albumsList = null;
    this._$albumPhotos = null;

    this.initView();
  }

  initView() {
    this._$albumsList = $(DIV_TEMPLATE)
      .addClass(ALBUMS_BOX_CLASS)
      .on('click', ALBUM_TITLE_SELECTOR, this.onAlbumClick.bind(this));

    this._$albumPhotos = $(DIV_TEMPLATE).addClass(PHOTOS_BOX_CLASS);

    this._$gallery = $(DIV_TEMPLATE)
      .addClass(GALLERY_CLASS)
      .append(this._$albumsList)
      .append(this._$albumPhotos);

    this._$root.append(this._$gallery);
  }

  renderAlbums(albums) {
    this._$albumsList.html(
      albums.map((album) => this.getAlbumHtml(album)).join('')
    );
    this.setActiveAlbum(1);
  }

  getAlbumHtml({ id, title }) {
    return ALBUM_TEMPLATE.replace(ALBUM_TITLE_PLACEHOLDER, title).replace(
      ALBUM_ID_PLACEHOLDER,
      id
    );
  }

  renderAlbumPhotos(photos) {
    this._$albumPhotos.html(
      photos.map((photo) => this.getPhotoHtml(photo)).join('')
    );
  }

  getPhotoHtml({ thumbnailUrl }) {
    return PHOTO_TEMPLATE.replace(PHOTO_URL_PLACEHOLDER, thumbnailUrl);
  }

  onAlbumClick(e) {
    const targetId = $(e.target).data('albumId');
    this.setActiveAlbum(targetId);
    this._actions.onAlbumChange(targetId);
  }

  setActiveAlbum(id) {
    $(ACTIVE_ALBUM_SELECTOR).toggleClass(ACTIVE_CLASS);
    $(DATA_SELECTOR.replace(ALBUM_ID_PLACEHOLDER, id)).addClass(ACTIVE_CLASS);
  }
}
