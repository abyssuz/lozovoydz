class Transport {
    constructor(name, color) {
      this.speed = 0;
      this.name = name;
      this.color = color;
    }

    ride(speed) {
      this.speed = speed;
      console.log(`${this.name}  едит со скоростью ${this.speed}.`);
    }

    stop() {
      this.speed = 0; 
      console.log(`${this.name} стоит.`);
     }
  }

class Car extends Transport {
    constructor(name, color) {
      super(name, color); // определяет свойства родителя
    }
}

let car = new Car('Машина', 'Красная');
  
class Boat extends Transport { // наследуем  свойства Animal , делаем bird  child Animal
    swim(speed) {
      this.speed = speed;
      console.log(`${this.name} плывет со скоростью ${this.speed} км/час.`);
    }
}
  
let boat = new Boat('Лодка', 'Белая');
  
class Airplane extends Transport {
    flight(speed) {
      this.speed = speed;
      console.log(`${this.name} летит со скоростью ${this.speed} км/час.`);
    }
}
  
let airplane = new Airplane('Самолет', 'Голубой')
  
console.log(car.name);
console.log(car.color);
car.stop();
car.ride(120);
  
console.log(boat.name);
console.log(boat.color);
boat.swim(60);
boat.stop();
  
console.log(airplane.name);
console.log(airplane.color);
airplane.flight(350)
airplane.stop();