const { parallel, src, dest } = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');

function buildJs() {
  return src(['./src/*/*.js', './src/main.js'])
    .pipe(concat('app.js'))
    .pipe(dest('./dist'));
}

function copyHtml() {
  return src('./src/index.html')
    .pipe(dest('./dist'));
}

function buildCss() {
  return src('./src/*.css')
    .pipe(concat('main.css'))
    .pipe(dest('./dist'));
}

function minifyJs() {
  return src(['./src/*/*.js', './src/main.js'])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(dest('./dist'));
}

function minifyCss() {
  return src('./src/*.css')
    .pipe(concat('main.css'))
    .pipe(cleanCSS())
    .pipe(dest('./dist'));
}

module.exports = {
  build: parallel(buildJs, buildCss, copyHtml),
  minify: parallel(minifyJs, minifyCss, copyHtml),
};
