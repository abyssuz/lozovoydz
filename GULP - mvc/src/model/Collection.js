class Collection {
  constructor(url) {
    this._url = url;
    this.list = [];
  }

  getList() {
    return fetch(this._url)
      .then((resp) => resp.json())
      .then((data) => (this.list = data));
  }

  deleteItem(id) {
    return fetch(`${this._url}/${id}`, {
      method: 'DELETE',
    }).then(() => (this.list = this.list.filter((item) => +item.id !== id)));
  }

  addItem(newItem) {
    return fetch(this._url, {
      method: 'POST',
      body: JSON.stringify(newItem),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((resp) => resp.json())
      .then((data) => {
          this.list.push(data)
          return data;
      });
  }

  toggleItem(id) {
    const toggledItem = this.list.find((item) => +item.id === id);
    toggledItem.isDone = !toggledItem.isDone;

    return fetch(`${this._url}/${id}`, {
      method: 'PUT',
      body: JSON.stringify(toggledItem),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((resp) => resp.json())
      .then(
        (data) => (this.list = this.list.map((item) =>
            item.id === data.id ? (item = data) : item
          ))
      );
  }
}
