const PHOTOS_URL = 'https://jsonplaceholder.typicode.com/photos?_limit=6';

function getPhotos() {
  return fetch(PHOTOS_URL)
    .then(resp => resp.json())
    .then(receivedPhotos => receivedPhotos.map(el => el.url))
    .then((urls) => $('#gallery').zoomy(urls));
}

getPhotos();