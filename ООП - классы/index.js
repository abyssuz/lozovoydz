class Hamburger {
    constructor(size) {
      this.price = size.price;
      this.callories = size.callories;
      this.addedToppings = [];
    }

    get burgerPrice() {
      return this.price;
    }

    get burgerCallories() {
      return this.callories;
    }

    get burgerToppings() {
      return this.addedToppings.join(',') || 'no toppings';
    }

    static SMALL_SIZE = {
      price: 50,
      callories: 20,
    }
    static MEDIUM_SIZE = {
      price: 75,
      callories: 30,
    }
    static BIG_SIZE = {
      price: 100,
      callories: 40,
    }
    static TOPPING_CHEESE = {
      name: 'cheese',
      price: 10,
      callories: 20,
    }
    static TOPPING_POTATO = {
      name: 'potato',
      price: 20,
      callories: 5,
    }
    static TOPPING_SALAD = {
      name: 'salad',
      price: 20,
      callories: 5,
    }
    static TOPPING_FLAVOURING = {
      name: 'flavouring',
      price: 15,
      callories: 0,
    }
    static TOPPING_MAYO = {
      name: 'mayo',
      price: 20,
      callories: 5,
    }

    addToppings(...toppings) {
      toppings.forEach((topping) => {
        this.price += topping.price;
        this.callories += topping.callories;
        this.addedToppings.push(topping.name);  
      })
   }
}

const burger = new Hamburger(Hamburger.BIG_SIZE);

console.log(`Big burger: price ${burger.burgerPrice}, callories ${burger.burgerCallories}`);

burger.addToppings(Hamburger.TOPPING_POTATO, 
                   Hamburger.TOPPING_MAYO,
                   Hamburger.TOPPING_CHEESE,
                   Hamburger.TOPPING_FLAVOURING);

console.log(`Big burger with ${burger.burgerToppings}: price ${burger.burgerPrice}, callories ${burger.burgerCallories}`);

const smallBurger = new Hamburger(Hamburger.SMALL_SIZE);

console.log(`Small burger: price ${smallBurger.burgerPrice}, callories ${smallBurger.burgerCallories}`);

smallBurger.addToppings(Hamburger.TOPPING_CHEESE,
                        Hamburger.TOPPING_CHEESE,
                        Hamburger.TOPPING_SALAD);

console.log(`Small burger with ${smallBurger.burgerToppings}: price ${smallBurger.burgerPrice}, callories ${smallBurger.burgerCallories}`);
