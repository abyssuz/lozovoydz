import React, { useState } from 'react';
import Chatbox from './Components/Chatbox';
import LoginWindow from './Components/LoginWindow';
import './main.css';

const App = () => {
  const [username, setUsername] = useState('');

  const onSubmitLogin = (username) => {
    setUsername(username);
  };

  return (
    <div className="chat-window">
      {username === '' ? (
        <LoginWindow onSubmitLogin={onSubmitLogin} />
      ) : (
        <Chatbox username={username} />
      )}
    </div>
  );
};

export default App;
