import React, { Component } from 'react';
import './Chatbox.css';
const url = 'wss://fep-app.herokuapp.com/';

class Chatbox extends Component {
  state = {
    username: this.props.username,
    messages: [],
    msgInput: '',
  };

  ws = new WebSocket(url);

  componentDidMount() {
    this.ws.onopen = () => {
      const query = {
        type: 'message',
        payload: {
          username: this.state.username,
          message: ' connected!',
        },
      };
      this.ws.send(JSON.stringify(query));
    };
    this.ws.onmessage = ({ data }) => {
      const resp = JSON.parse(data);
      this.setState({
        ...this.state,
        messages: [...this.state.messages, resp],
      });
    };
  }

  sendMsg() {
    const query = {
      type: 'message',
      payload: {
        username: this.state.username,
        message: this.state.msgInput,
      },
    };
    this.ws.send(JSON.stringify(query));
  }

  render() {
    return (
      <div className="chatbox">
        <div className="msg-field">
          {this.state.messages.map((msg, index) => (
            <p
              className="msg-text"
              key={index}
            >{`${msg.payload.username}: ${msg.payload.message}`}</p>
          ))}
        </div>
        <form
          className="msg-form"
          onSubmit={(e) => {
            e.preventDefault();
            this.sendMsg(this.state.msgInput);
            this.setState({ ...this.state, msgInput: '' });
          }}
        >
          <input
            className="msg-input"
            value={this.state.msgInput}
            onChange={(e) => {
              this.setState({ ...this.state, msgInput: e.target.value });
            }}
          ></input>
        </form>
      </div>
    );
  }
}

export default Chatbox;
