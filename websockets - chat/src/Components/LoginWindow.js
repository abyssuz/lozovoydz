import React, { useState } from 'react';
import './LoginWindow.css';

const LoginWindow = ({ onSubmitLogin }) => {
  const [username, setUsername] = useState('');

  return (
    <div className="login-window">
      <p className="login-label">Enter your nickname</p>
      <form
        className="login-form"
        onSubmit={(e) => {
          e.preventDefault();
          onSubmitLogin(username);
        }}
      >
        <input
          className="login-input"
          placeholder="Your nickname..."
          onChange={(e) => {
            setUsername(e.target.value);
          }}
        ></input>
        <input className="login-btn" type="submit" value="OK"></input>
      </form>
    </div>
  );
};

export default LoginWindow;
