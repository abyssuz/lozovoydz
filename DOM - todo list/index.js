const tasksList = document.getElementById('list-container');
const taskInput = document.getElementById('task-input');
const taskButton = document.getElementById('add-task-button');

taskButton.addEventListener('click', addTask);

function addTask(){
    if(taskInput.value !== ''){
        const newTask = createTask();
        tasksList.append(newTask);
    } else
        alert('Input is empty!')
}

function createTask(){        
    const taskPoint = document.createElement('div');
    const taskText = document.createElement('p');
    const taskRemoveButton = document.createElement('div');

    taskPoint.classList.add('list-item');
    taskText.classList.add('task-text');
    taskRemoveButton.classList.add('task-remove-button');

    taskText.textContent = taskInput.value;
    taskInput.value = '';
    addRemoveEvent(taskRemoveButton);

    taskPoint.append(taskText);
    taskPoint.append(taskRemoveButton);

    
    return taskPoint;
}

function addRemoveEvent(removeButton){
    removeButton.addEventListener('click', () => {
        removeButton.parentElement.remove();
    })
}

