const TASK_TEXT_PLACEHOLDER = '{{tasktext}}';
const TASK_ID_PLACEHOLDER = '{{id}}';
const TASK_DONE_PLACEHOLDER = '{{isDone}}';
const TASK_REMOVE_BTN_CLASS = 'task-remove-button';
const TASK_ITEM_CLASS = 'list-item';
const TASK_TEXT_CLASS = 'task-text';
const TASK_ID_ATTRIBUTE_NAME = 'data-task-id';

const tasksList = document.getElementById('list-container');
const taskInput = document.getElementById('task-input');
const newTaskTemplate = document.getElementById('newTask-template').innerHTML;

let todoTasks = [];

document.getElementById('add-task-button')
        .addEventListener('click', onAddTaskBtnClick);

tasksList.addEventListener('click', onTodoListClick);

function onAddTaskBtnClick() {
    if(!isEmpty(taskInput.value)){
        const newTask = getNewTask();

        addNewTask(newTask);
        renderTask(newTask);

        resetInput();
    } else
        alert('Input is empty!')
}

function onTodoListClick(e) {
    let taskId = 0;

    if(isRemoveButtonClicked(e.target)){
        taskId = getTaskId(e.target);
        removeTask(taskId);
        removeTaskElement(taskId);
    } else if(isTaskClicked(e.target)){
        taskId = getTaskId(e.target);
        changeTaskState(taskId);
        renderTaskState(taskId);
    }
}

function isEmpty(str) {
    return str === '' || str === null;
}

function isRemoveButtonClicked(target) {
    return target.classList.contains(TASK_REMOVE_BTN_CLASS);
}

function isTaskClicked(target){
    return target.classList.contains(TASK_ITEM_CLASS) || 
           target.classList.contains(TASK_TEXT_CLASS);
}

function getNewTask() {
    return {
        id : Date.now(),
        taskText : taskInput.value,
        isDone : false, 
    }
}

function addNewTask(newTask) {       
    todoTasks.push(newTask); 
    saveToStorage();
}

function renderTask(task) {
    const taskHtml = getTaskHtml(task);
    
    tasksList.insertAdjacentHTML('beforeend', taskHtml);
}

function getTaskHtml(task) {
    const taskTemplate = getTaskState(task);

    return taskTemplate.replace(TASK_ID_PLACEHOLDER, task.id)
                       .replace(TASK_TEXT_PLACEHOLDER, task.taskText);
}

function getTaskState(task) {
    if(task.isDone)
        return newTaskTemplate.replace(TASK_DONE_PLACEHOLDER, 'done');
    else
        return newTaskTemplate.replace(TASK_DONE_PLACEHOLDER, '');
}

function resetInput() {
    taskInput.value = '';
}

function getTaskId(element) {
    const task = element.closest('.' + TASK_ITEM_CLASS);

    return +task.dataset.taskId;
}

function removeTask(id) {
    todoTasks = todoTasks.filter(element => element.id !== id);
    saveToStorage();
}

function removeTaskElement(id) {
    getTaskElementById(id).remove();
}

function changeTaskState(id) {
    const task = getTaskById(id);

    if(task.isDone)
        task.isDone = false;
    else
        task.isDone = true;
    saveToStorage();
}

function renderTaskState(id){
    const taskElement = getTaskElementById(id);

    taskElement.classList.toggle('done');
}

function getTaskById(id) {
    return todoTasks.find(element => element.id === id);
}

function getTaskElementById(id) {
    return tasksList.querySelector(`[${TASK_ID_ATTRIBUTE_NAME}="${id}"]`);
}

function saveToStorage() {
    localStorage.setItem('todoList', JSON.stringify(todoTasks));
}

function restoreFromStorage(){
    const storageTodoList = localStorage.getItem('todoList');

    if(storageTodoList !== null) {
        todoTasks = JSON.parse(storageTodoList);
    } else {
        todoTasks = [];
    }
}

function init() {
    restoreFromStorage();
    renderTodoList();
}

function renderTodoList() {
    todoTasks.forEach(task => {
        renderTask(task);
    });
}

init();