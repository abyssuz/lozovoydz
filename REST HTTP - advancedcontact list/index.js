class ContactList {
  constructor(contactsTable) {
    this.list = [];
    this.selectedContact = {};

    this.contactTable = contactsTable.querySelector('#contact-table');
    this.nameInput = contactsTable.querySelector('#name-input');
    this.emailInput = contactsTable.querySelector('#email-input');
    this.phoneInput = contactsTable.querySelector('#phone-input');
    this.addContactForm = contactsTable.querySelector('#add-contact-form');
    this.addButton = contactsTable.querySelector('#add-contact-button');
    this.deleteButton = contactsTable.querySelector('#delete-contact-button');
    this.editButton = contactsTable.querySelector('#edit-contact-button');

    this.addContactForm.addEventListener('submit', this.onFormSubmit.bind(this));
    this.contactTable.addEventListener('click', this.onContactTableCliked.bind(this));
    this.deleteButton.addEventListener('click', this.onDeleteButtonClicked.bind(this));
    this.editButton.addEventListener('click', this.onEditButtonClicked.bind(this));
  }

  onFormSubmit(e) {
    e.preventDefault();
    this.addNewContact();
  }

  onContactTableCliked(e) {
    this.selectContact(e.target);
  }

  onDeleteButtonClicked() {
    this.deleteContact();
  }

  onEditButtonClicked() {
    if(this.editButton.classList.contains('save-edit')) {
      this.saveEditContact();
      this.switchEditButton();
    } else {
      this.editContact();
    }
    this.switchButtons(this.addButton, this.deleteButton);
  }

  init() {
    this.getContacts().then(() => {
      this.renderContacts();
    });
  }

  getContacts() {
    return fetch(ContactList.CONTACTS_URL)
      .then((resp) => resp.json())
      .then((contacts) => (this.list = contacts));
  }

  renderContacts() {
    const listHtml = this.list
      .map((contact) => this.getContactHtml(contact))
      .join('\n');

    this.contactTable.insertAdjacentHTML('beforeend', listHtml);
  }

  getContactHtml(contact) {
    return ContactList.contactTemplate
      .replace(ContactList.NAME_PLACEHOLDER, contact.name)
      .replace(ContactList.EMAIL_PLACEHOLDER, contact.email)
      .replace(ContactList.PHONE_PLACEHOLDER, contact.phone)
      .replace(ContactList.ID_PLACEHOLDER, contact.id);
  }

  addNewContact() {
    if (this.isFormValid()) {
      const newContact = this.getFormData();

      this.addContactRequest(newContact)
        .then((resp) => resp.json())
        .then((data) => {
          this.list.push(data);
          this.renderNewContact(data);
        });
      this.resetForm();
    } else {
      return;
    }
  }

  isFormValid() {
    return (
      this.nameInput.value !== '' &&
      this.emailInput.value !== '' &&
      this.phoneInput.value !== ''
    );
  }

  getFormData() {
    return {
      name: this.nameInput.value,
      email: this.emailInput.value,
      phone: this.phoneInput.value,
    };
  }

  addContactRequest(newContact) {
    return fetch(ContactList.CONTACTS_URL, {
      method: 'POST',
      body: JSON.stringify(newContact),
      headers: { 'Content-Type': 'application/json' },
    });
  }

  renderNewContact(contact) {
    const newContactHtml = this.getContactHtml(contact);

    this.contactTable.insertAdjacentHTML('beforeend', newContactHtml);
  }

  resetForm() {
    this.nameInput.value = '';
    this.emailInput.value = '';
    this.phoneInput.value = '';
  }

  selectContact(tableRow) {
    this.isSelected() && this.unselectContact();

    if (tableRow.closest('.contact')) {
      this.selectedContact.element = tableRow.closest('.contact');
      this.selectedContact.contact = this.list.find(
        (contact) => contact.id === this.selectedContact.element.dataset.contactId
      );
      this.switchActiveContact();
    }
  }

  isSelected() {
    return Object.keys(this.selectedContact).length !== 0;
  }

  unselectContact() {
    this.switchActiveContact();
    this.selectedContact = {};
  }

  switchActiveContact() {
    this.selectedContact.element.classList.toggle('active');
  }

  deleteContact() {
    this.deleteContactRequest().then(() => {
      this.list = this.list.filter(
        (contact) => contact.id !== this.selectedContact.id
      );
      this.selectedContact.element.remove();
      this.selectedContact = {};
    });
  }

  deleteContactRequest() {
    return fetch(ContactList.CONTACTS_URL + this.selectedContact.contact.id, {
      method: 'DELETE',
    });
  }

  editContact() {
    this.setFormData(this.selectedContact.contact);
    this.switchEditButton();
  }

  setFormData({ name, email, phone }) {
    this.nameInput.value = name;
    this.emailInput.value = email;
    this.phoneInput.value = phone;
  }

  switchEditButton() {
    if(this.editButton.classList.contains('save-edit'))
      this.editButton.innerText = 'Edit contact';
    else 
      this.editButton.innerText = 'Save edit';

    this.editButton.classList.toggle('save-edit');
  }

  saveEditContact() {
    const editedContact = this.getFormData();

    this.editContactRequest(editedContact)
      .then((resp) => resp.json())
      .then((data) => this.updateEditedContact(data));
  }

  editContactRequest(editedContact) {
    return fetch(ContactList.CONTACTS_URL + this.selectedContact.contact.id, {
      method: 'PUT',
      body: JSON.stringify(editedContact),
      headers: { 'Content-Type': 'application/json' },
    });
  }

  updateEditedContact(contact) {
    const oldContactIndex = this.list.findIndex(
      (item) => item.id === contact.id
    );

    this.list.splice(oldContactIndex, 1, contact);
    this.selectedContact.element.innerHTML = this.getContactHtml(contact);

    this.selectedContact.element.classList.toggle('active');
    this.resetForm();
    this.selectedContact = {};
  }

  switchButtons(...buttons) {
    buttons.forEach(button => {
      if (button.disabled) button.disabled = false;
      else button.disabled = true;
    })
  }

  static contactTemplate = document.getElementById('contact-template').innerHTML;
  static CONTACTS_URL = 'https://5dd3d5ba8b5e080014dc4bfa.mockapi.io/users/';
  static NAME_PLACEHOLDER = '{{name}}';
  static EMAIL_PLACEHOLDER = '{{email}}';
  static PHONE_PLACEHOLDER = '{{phone}}';
  static ID_PLACEHOLDER = '{{id}}';
}

const contactsTableEl = document.getElementById('contact-list');
const contactsList = new ContactList(contactsTableEl);

contactsList.init();