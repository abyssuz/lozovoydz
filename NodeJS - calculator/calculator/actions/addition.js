const addition = (...operands) => {
  return operands.reduce((acc, operand) => {
    return acc + operand;
  });
};

module.exports = {
  addition,
};
