const { addition } = require('./actions/addition');
const { substruction } = require('./actions/substruction');
const { multiplication } = require('./actions/multiplication');
const { division } = require('./actions/division');

module.exports = {
  add: addition,
  sub: substruction,
  mult: multiplication,
  div: division,
};
