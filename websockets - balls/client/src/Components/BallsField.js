import React from 'react';
import Ball from './Ball';

const BallsField = ({ onFieldClick, ballsList }) => {
  return (
    <div className="balls-field" onClick={(e) => onFieldClick(e)}>
      {ballsList.map((ballOptions) => (
        <Ball key={ballOptions.id} options={ballOptions} />
      ))}
    </div>
  );
};

export default BallsField;
