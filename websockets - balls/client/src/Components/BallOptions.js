import React, { useState } from 'react';

const BallOptions = ({ onBallUpdate }) => {
  const [colorInput, setColorInput] = useState('#000000');
  const [sizeInput, setSizeInput] = useState(0);

  return (
    <div className="ball-options">
      <input
        type="color"
        onChange={(e) => setColorInput(e.target.value)}
      ></input>
      <input
        type="number"
        onChange={(e) => setSizeInput(e.target.value)}
      ></input>
      <input
        type="button"
        value="Enter"
        onClick={() => onBallUpdate({color: colorInput, size: +sizeInput})}
      ></input>
    </div>
  );
};

export default BallOptions;
