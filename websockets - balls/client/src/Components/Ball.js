import React from 'react';

const Ball = ({ options: {  top, left, size, color } }) => {
  return (
    <div
      className="ball"
      style={{
        top: top-size/2,
        left: left-size/2,
        width: size,
        height: size,
        backgroundColor: color,
      }}
    >
    </div>
  );
};

export default Ball;
