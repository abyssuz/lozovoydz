export default class MySocket {
  constructor() {
    this.socket = new WebSocket(MySocket.url);
  }

  onOpen(ball) {
    this.socket.onopen = () => {
      const query = {
        type: 'add',
        payload: ball,
      };
      this.socket.send(JSON.stringify(query));
    };
  }

  onMessage(onMsg) {
    this.socket.onmessage = ({data}) => {
      onMsg(JSON.parse(data));
    };
  }

  sendUpdate(update) {
    const query = {
      type: 'update',
      payload: update,
    };
    this.socket.send(JSON.stringify(query));
  }

  static url = 'wss://balls-fep.herokuapp.com/';
}
