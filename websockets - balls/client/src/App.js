import React, { useEffect, useState } from 'react';
import BallOptions from './Components/BallOptions';
import BallsField from './Components/BallsField';
import MySocket from './socket/MySocket';
import './main.css';
const ws = new MySocket();

const App = () => {
  const [ballsList, setBallsList] = useState([]);
  const [ballOptions, setBallOptions] = useState({
    id: Math.random(),
    top: 100,
    left: 100,
    color: '#000000',
    size: 100,
  });

  const onBallUpdate = (updates) => {
    const updatedBall = { ...ballOptions, ...updates };
    if (ws.socket.readyState === 1) {
      ws.sendUpdate(updatedBall);
      setBallOptions(updatedBall);
    }
  };

  const onFieldClick = (e) => {
    if (e.target.classList.contains('balls-field')) {
      const rect = e.target.getBoundingClientRect();
      onBallUpdate({
        top: e.clientY - rect.y,
        left: e.clientX - rect.x,
      });
    }
  };

  useEffect(() => {
    const onMsg = ({ type, payload }) => {
      setBallsList((ballsList) => {
        switch (type) {
          case 'add':
            return [...ballsList, payload];
          case 'update':
            return ballsList.map((ball) =>
              ball.id === payload.id ? { ...ball, ...payload } : ball
            );
          case 'remove':
            return ballsList.filter((ball) => ball.id !== payload.id);
        }
      });
    };

    ws.onOpen(ballOptions);
    ws.onMessage(onMsg);
  }, []);

  return (
    <div className="balls">
      <BallOptions onBallUpdate={onBallUpdate} />
      <BallsField ballsList={ballsList} onFieldClick={onFieldClick} />
    </div>
  );
};

export default App;
