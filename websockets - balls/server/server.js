const express = require('express');
const { Server } = require('ws');
const PORT = process.env.PORT || 4000;

const server = express()
  .get('/', function (req, res) {
    res.send({ response: 'I am alive' });
  })
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

const wss = new Server({ server });

let usersBalls = [];

wss.broadcast = function (message) {
  wss.clients.forEach(function (client) {
    client.send(message);
  });
};

wss.on('connection', function connection(ws) {
  if (wss.clients.size >= 1) {
    usersBalls.forEach((user) => {
      ws.send(JSON.stringify({ ...user, type: 'add' }));
    });
  }

  ws.on('message', function (message) {
    const msg = JSON.parse(message);
    switch (msg.type) {
      case 'add':
        usersBalls = [...usersBalls, msg];
        ws.id = msg.payload.id;
        break;
      case 'update':
        usersBalls = usersBalls.map((user) =>
          user.payload.id !== msg.payload.id ? user : { ...user, ...msg }
        );
    }
    wss.broadcast(JSON.stringify(msg));
  });

  ws.on('close', function () {
    const removedBall = usersBalls.find((user) => user.payload.id == ws.id);
    wss.broadcast(
      JSON.stringify({
        type: 'remove',
        payload: { id: removedBall.payload.id },
      })
    );
    usersBalls = usersBalls.filter((user) => user.payload.id != ws.id);
  });
});
